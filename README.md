# Docker traefik

This docker acts as a reverse-proxy for other containers

#### Create and run home docker as detached
    docker-compose up -d

#### Create and run home docker
    docker-compose up

#### Stop home docker and related network
    docker-compose down

#### Access to logs and follow
    docker-compose logs -f

#### Pull image
    docker-compose pull

#### Open terminal on docker container
    docker containerid exec -it dockname /bin/bash

You just have to had a fews labels and acces to traefik network in other containers that traefik is gonna discover through docker-socket-proxy (which uses docker sockets)

To define `${DOMAIN}`, you can add a .env file next to your docker-compose file with your domain (without www):
```
DOMAIN=domain.tld
```

Docker Example: 
```
service
  docker-example:
    labels:
      traefik.enable: 'true'
      traefik.http.routers.http_my-service-name.entrypoints: http
      traefik.http.routers.http_my-service-name.rule: Host(`${DOMAIN}`,`www.${DOMAIN}`)
      traefik.http.routers.http_my-service-name.middlewares: https@file

      traefik.http.routers.https_my-service-name.entrypoints: https
      traefik.http.routers.https_my-service-name.tls: 'true'
      traefik.http.routers.https_my-service-name.rule: Host(`${DOMAIN}`,`www.${DOMAIN}`)
      traefik.http.routers.https_my-service-name.middlewares: non-www@file,error-pages #you wan remove 'non-www' middleware if you do not want www.domain.tld->domain.tld
      traefik.http.routers.https_my-service-name.tls.certresolver: letsencrypt
      traefik.http.services.my_service_name.loadbalancer.server.port: 9999   #Needed is no port(other than 80) or several ports are exposed by docker
     networks:
       - traefik_net

networks:
  traefik_net:
    external: true
```
variable 'my-service-name' can be what ever you want as soon it is different between docker containers

Replace the email by yours in the traefik conf for letsencrypt (conf/traefik.yaml)
Replace the domain by yours in .env file in your new service

A simple label is used to handle error pages from other containers:
(https://github.com/tarampampam/error-pages)

```
labels:
  - traefik.http.routers.my-service-name.middlewares=error-pages@docker
```

Currently there is 2 middlewares defined in traefik dynamic conf. If applied as in the example, you can redirect as follow:
- http://domain.tld      -> [https_middleware] -> https:/domain.tld      -> [non-www_middleware] -> https://domain.tld
- http://www.domain.tld  -> [https_middleware] -> https://www.domain.tld -> [non-www_middleware] -> https://domain.tld
- https://domain.tld     -> [https_middleware] -> https:/domain.tld      -> [non-www_middleware] -> https://domain.tld
- https://www.domain.tld -> [https_middleware] -> https://www.domain.tld -> [non-www_middleware] -> https://domain.tld
